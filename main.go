package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/pantacor/pantahub-listen/controllers"
)

func main() {

	buildName := filepath.Base(os.Args[0])

	if strings.HasSuffix(buildName, "1") {
		ns := controllers.NewDeviceProcessor()
		ns.Run()
	} else if strings.HasSuffix(buildName, "2") {
		ns := controllers.NewStepProcessor()
		ns.Run()
	} else {
		fmt.Println("Invalid topic(Note:Use 1  or 2 as the suffix of your binary name,eg:go build -o=topic1,go build -o=topic2)")
	}
}
