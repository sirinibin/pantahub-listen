# Pantahub Listen

## Build & Run

> Note:Use 1 or 2 as suffix of your build name.
>
> > 1 => topic : pantabasemgo.pantabase-serv.pantahub_devices
> > 2 => topic : pantabasemgo.pantabase-serv.pantahub_steps

```
$ go build -o=topic1
$ ./topic1
kafka-logger: entering loop for consumer group, consumer-group-objcontrol-devices
kafka-logger: joined group consumer-group-objcontrol-devices as member test1@lenovo-ThinkPad-P50 (github.com/segmentio/kafka-go)-4cbdc2ce-5628-4a91-8543-e52c6387ca2d in generation 111
kafka-logger: selected as leader for group, consumer-group-objcontrol-devices
kafka-logger: using 'range' balancer to assign group, consumer-group-objcontrol-device
...

$ go build -o=topic2
$ ./topic2
kafka-logger: entering loop for consumer group, consumer-group-objcontrol-steps
kafka-logger: joined group consumer-group-objcontrol-steps as member test2@lenovo-ThinkPad-P50 (github.com/segmentio/kafka-go)-61a39232-8154-4331-b2b8-359f5a00fe54 in generation 100
kafka-logger: selected as leader for group, consumer-group-objcontrol-steps
kafka-logger: using 'range' balancer to assign group, consumer-group-objcontrol-steps

```

## Topics available

```
1.pantabasemgo.pantabase-serv.pantahub_devices
2.pantabasemgo.pantabase-serv.pantahub_steps

```
