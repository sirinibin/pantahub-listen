module gitlab.com/pantacor/pantahub-listen

go 1.13

replace github.com/ant0ine/go-json-rest => github.com/asac/go-json-rest v3.3.3-0.20191004094541-40429adaafcb+incompatible

replace github.com/go-resty/resty => gopkg.in/resty.v1 v1.11.0

require (
	github.com/segmentio/kafka-go v0.3.5
	gitlab.com/pantacor/pantahub-base v0.0.0-20200423214821-032eda712050
	gopkg.in/resty.v1 v1.12.0
)
